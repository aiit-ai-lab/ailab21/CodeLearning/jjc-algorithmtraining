/*
给你两个字符串：ransomNote 和 magazine ，判断 ransomNote 能不能由 magazine 里面的字符构成。

如果可以，返回 true ；否则返回 false 。

magazine 中的每个字符只能在 ransomNote 中使用一次。

示例 1：

输入：ransomNote = "a", magazine = "b"
输出：false
示例 2：

输入：ransomNote = "aa", magazine = "ab"
输出：false
示例 3：

输入：ransomNote = "aa", magazine = "aab"
输出：true
 

提示：

1 <= ransomNote.length, magazine.length <= 105
ransomNote 和 magazine 由小写英文字母组成
*/
#include<iostream>
#include<string>
using namespace std;
class Solution {
public:
    bool canConstruct(string ransomNote, string magazine) 
    {
        int save[26]={0};
        int lenr=ransomNote.length();
        int lenm=magazine.length();
        for(int i=0;i<lenm;i++)
            save[25-'z'+magazine[i]]+=1;

        for(int i=0;i<lenr;i++)
        {
            save[25-'z'+ransomNote[i]]-=1;

            if(save[25-'z'+ransomNote[i]]<0)
                return false;
        }
        return true;
    }
};
int main()
{
    string ransomNote,magazine;
    cin>>ransomNote>>magazine;
    Solution S;
    cout<<(S.canConstruct(ransomNote,magazine)?"Ture":"false")<<endl;
    return 0;
}