/*
本题要求你模拟星际队的行进轨迹，以边长2n+1的矩形为战略地图，从左上角出发以反'S'形蛇皮走位抵达右下角，输出用26个小写字母循环表示。
输入描述:
多组输入(不多于10组)，每行一个整数n(0 <= n <= 100)。
输出描述:
对于每组数据，输出一个(2n+1)*(2n+1)的字母矩阵。
示例1
输入:
1
2
3
输出:
abc
fed
ghi
abcde
jihgf
klmno
tsrqp
uvwxy
abcdefg
nmlkjih
opqrstu
bazyxwv
cdefghi
ponmlkj
qrstuvw
*/
#include<iostream>
#include<vector>
using namespace std;
int main()
{
    int count;
    while(cin>>count)
    {
        vector<vector<char>>answer;
        int n=count;
        n=2*n+1;
        answer.resize(n);
        char ele='a';
        int judge=0;
        for(int i=0;i<n;i++)
        {
            if(judge%2==0)
            {
                for(int j=0;j<n;j++)
                {
                    answer[i].emplace_back(ele);
                    ele++;
                    if(ele=='z'+1)
                    ele='a';
                }
            }
            else
            {
                for(int j=0;j<n;j++)
                {
                    answer[i].emplace(answer[i].begin(),ele);
                    ele++;
                    if(ele=='z'+1)
                    ele='a';
                }
            }
            judge++;
        }
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<n;j++)
            cout<<answer[i][j];
            cout<<endl;
        }
    }
    return 0;
}