#include<iostream>
using namespace std;
const int Amax = 1e6 + 5;
int a[Amax],q[Amax];
int main()
{
    int n,k;
    cin>>n>>k;
    for(int i=1;i<=n;i++)
        cin>>a[i];
    int left=1,right=0;
    for(int j=1;j<=n;j++)
    {
        while(left<=right&&a[q[right]]>a[j])
        right--;
        while(left<=right&&q[left]+k<=j)
        left++;
        q[++right]=j;
        if(j>=k)
        cout<<a[q[left]]<<" ";
    }
    cout<<endl;
    left=1,right=0;
    for(int p=1;p<=n;p++)
    {
        while(left<=right&&a[q[right]]<a[p])
        right--;
        while(left<=right&&q[left]+k<=p)
        left++;
        q[++right]=p;
        if(p>=k)
        cout<<a[q[left]]<<" ";
    }
    return 0;
}