/*
链接：https://ac.nowcoder.com/acm/contest/45864/A
来源：牛客网

题目描述 
在一个排列中，如果一对数的前后位置与大小顺序相反，即前面的数大于后面的数，那么它们就称为一个逆序。一个排列中逆序的总数就称为这个排列的逆序数。比如一个序列为4 5 1 3 2， 那么这个序列的逆序数为7，逆序对分别为(4, 1), (4, 3), (4, 2), (5, 1), (5, 3), (5, 2),(3, 2)。
输入描述:
第一行有一个整数n(1 <= n <= 100000),  然后第二行跟着n个整数，对于第i个数a[i]，(0 <= a[i] <= 100000)。
输出描述:
输出这个序列中的逆序数
示例1
输入
5
4 5 1 3 2
输出
7
*/
#include<iostream>
using namespace std;
int a[100010]={0},b[100010]={0};
long long Merge_sort(int L,int R)
{
    if(L>=R)
        return 0;
    int mid=(L+R)/2;
    long long result=Merge_sort(L, mid)+Merge_sort(mid+1, R);
    int i=L,j=mid+1,k=0;
    while(i<=mid&&j<=R)
    {
        if(a[i]<=a[j])
            b[k++]=a[i++];
        else
        {
            result+=mid-i+1;
            b[k++]=a[j++];
        }
    }
    while(i<=mid)
        b[k++]=a[i++];
    while(j<=R)
        b[k++]=a[j++];
    for(i=L,j=0;i<=R;i++,j++)
        a[i]=b[j];
    return result;
}
int main()
{
    int n;
    cin>>n;
    for(int i=0;i<n;i++)
        cin>>a[i];
    cout << Merge_sort(0, n-1);
    return 0;
}