/*
链接：https://ac.nowcoder.com/acm/problem/20443
来源：牛客网

题目描述 
某人读论文，一篇论文是由许多单词组成。但他发现一个单词会在论文中出现很多次，现在想知道每个单词分别在论文中出现多少次。
输入描述:
第一个一个整数N,表示有多少个单词，接下来N行每行一个单词。每个单词由小写字母组成，N ≤ 200,单词长度不超过10^6
输出描述:
输出N个整数，第i行的数字表示第i个单词在文章中出现了多少次。
示例1
输入
复制
3
a
aa
aaa
输出
复制
6
3
1
备注:
30%的数据， 单词总长度不超过 
1
0
3
10 
3
 。
100%的数据，
1
≤
�
≤
200
1≤n≤200，单词总长度不超过 
1
0
6
10 
6
 。
*/
#include<iostream>
#include<string>
using namespace std;
const int Tmax = 1e6+5;
string All[Tmax];
int N, AN, Ai = 0, Bi = 0, answer = 0;
string str;

void get_next(int *next, string S, int size)
{
    next[0] = 0;
    int cpas = 0;
    int i = 1;
    while(i < size)
    {
        if(S[cpas] == S[i])
        {
            cpas++;
            next[i] = cpas;
            i++;
        }
        else
        {
            if(cpas == 0)
            {
                next[i] = 0;
                i++;
            }
            else
                cpas = next[cpas - 1];
        }
    }
}

void KMP_judgement()
{
    answer = 0;
    str = All[Bi];
    Bi++;
    int size = str.size();
    int next[size];
    get_next(next, str, size);
    int MAIN, KID;
    for(int i = 0; i < N; i++)
    {
        MAIN = 0, KID = 0;
        if(size > All[i].size())
            continue;
        while(MAIN < All[i].size())
        {
            if(str[KID] == All[i][MAIN])
            {
                MAIN++;
                KID++;
            }
            else if(KID > 0)
                KID = next[KID - 1];
            else
                MAIN++;

            if(KID == size)
                answer++;
        }
    }
    cout << answer <<endl;
}

int main()
{
    cin >> N;
    AN = N;
    while(AN--)
    {
        cin >> All[Ai];
        Ai++;
    }
    AN = N;
    while(AN--)
        KMP_judgement();
    return 0;
}