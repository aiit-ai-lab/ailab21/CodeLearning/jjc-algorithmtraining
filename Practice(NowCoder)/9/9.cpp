#include<iostream>
using namespace std;
const int Tmax=2e5+10;
int T,n,k,score[Tmax],maxscore=0,aomaxscore=0,Park=0,judgement=0,answer=0;
int main()
{
    cin>>T;
    while(T--)
    {
        cin>>n>>k;
        for(int i=0;i<n;i++)
        cin>>score[i];
        for(int i=0;i<k;i++)
        maxscore+=score[i];
        aomaxscore=maxscore;
        for(int i=k;i<n;i++)
        {
            if((maxscore-score[i-1]+score[i])>maxscore)
            {
                maxscore=(maxscore-score[i-1]+score[i]);
                Park=i;
            }
        }
        for(int i=k;i<n;i++)
        {
            if(i==Park+1-k)
            {
                if(Park+1>n-k)
                break;
                for(i=Park+1;i<Park+1+k;i++)
                judgement+=score[i];
                i--;
                if(judgement>aomaxscore)
                aomaxscore=judgement;
                continue;
            }
            if((aomaxscore-score[i-1]+score[i])>aomaxscore)
                aomaxscore=(aomaxscore-score[i-1]+score[i]);
        }
        answer=maxscore+aomaxscore;
        cout<<answer<<endl;
    }
    return 0;
}