/*

//顺序栈

#include <iostream>
using namespace std;
struct Test
{
    int *base;
    int *top;
}T;
void InitStack(Test &one)
{
    one.base = new int[10];
    if(!one.base)
    exit(0);
    one.top = one.base;
    for(int i = 0; i < 10; i++)
    {
        *one.top = 0;
        one.top++;
    }
    one.top = one.base;
}
void Push(Test &one, int e)
{
    if(one.top - one.base == 10)
    exit(0);
    *one.top = e;
    one.top++;
}
void Pop(Test &one)
{
    if(one.top == one.base)
    exit(0)
}
int main()
{
    InitStack(T);
    Push(T, 5);
    int *p = T.base;
    for(int i = 0; i < 10; i++)
    {
        cout << *p <<endl;
        p++;
    }
    return 0;
}
*/



//链栈

#include<iostream>
using namespace std;
struct StackNode
{
    int data;
    StackNode *next;
};

void InitStack(StackNode* &one)
{
    one = new StackNode;
    one = nullptr;
}

void Push(StackNode* &one, int e)
{
    StackNode* P = new StackNode;
    P -> data = e;
    P -> next = one;
    one = P;
}

void Pop(StackNode* &one, int num)
{
    StackNode* p1 = one;
    StackNode* p2 = one;
    if(num == one -> data)
    {
        one = one -> next;
        delete p1, p2;
        p1, p2 = nullptr;
        return;
    }
    while(num != p1 -> data)
    {
        p2 = p1;
        p1 = p1 -> next;
    }
    p2 -> next = p1 -> next;
    delete p1;
    p1 = nullptr;
}

int main()
{
    StackNode* sn1;
    InitStack(sn1);
    Push(sn1, 5);
    Push(sn1, 6);
    Push(sn1, 7);
    Push(sn1 ,8);
    Push(sn1, 9);
    Pop(sn1, 9);
    Pop(sn1, 8);
    Pop(sn1, 6);
    while(sn1 != nullptr)
    {
        cout << sn1 -> data << endl;
        sn1 = sn1 -> next;
    }
    delete sn1;
    sn1 = nullptr;
    return 0;
}