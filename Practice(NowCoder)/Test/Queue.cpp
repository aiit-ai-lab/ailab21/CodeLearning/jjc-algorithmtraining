#include<iostream>
using namespace std;

/*
//循环队列

struct Queue
{
    int* base;
    int front;
    int rear;
};

void InitQueue(Queue* &one)
{
    one = new Queue;
    one -> base = new int[6];
    one -> front = one -> rear = 0;
}

//在队尾添加一个新的元素
void Push(Queue* &one, int e)
{
    if((one -> rear + 1) % 6 == one -> front)
    {
        cout << u"队列已满" <<endl;
        exit(0);
    }
    one -> base[one -> rear] = e;
    one -> rear = (one -> rear + 1) % 6; 
}

//将队头元素删除(不是真正删除，只是指针后移，输出不到队头)
void Pop(Queue* &one)
{
    if(one -> front == one -> rear)
    {
        cout << u"队列已空" <<endl;
        exit(0);
    }
    one -> front = (one -> front + 1) % 6;
}

void Print(Queue* one)
{
    int F = one -> front;
    while((one -> front % 6) != one -> rear)
    {
        cout << one -> base[one -> front % 6] <<endl;
        one -> front++;
    }
    //便于重复输出
    one -> front = F;
}

int main()
{
    Queue* Q;
    InitQueue(Q);
    Push(Q, 1);
    Push(Q, 2);
    Push(Q, 3);
    Push(Q, 4);
    Push(Q, 5);
    Print(Q);
    cout << "-----------------------------------------" <<endl;
    Pop(Q);
    Pop(Q);
    Push(Q, 6);
    Print(Q);
    cout << "-----------------------------------------" <<endl;
    Pop(Q);
    Push(Q, 7);
    Print(Q);
    delete[] Q -> base;
    Q -> base = nullptr;
    delete Q;
    Q = nullptr;
    return 0;
}

*/


//链队

struct QueueNode
{
    int data;
    QueueNode* next;
    //QueueNode* front;
    //QueueNode* rear;
};
//两结构体可以放在一起，为方便学习与查看遂分开写
struct LinkQueue
{
    QueueNode* front;
    QueueNode* rear;
};

void InitQueue(LinkQueue* &one)
{
    one = new LinkQueue;
    one -> front = new QueueNode;
    one -> rear = one -> front;
    one -> front -> next = nullptr;
}

//在链队尾部添加元素
void Push(LinkQueue* &one, int e)
{
    QueueNode* p = new QueueNode;
    p -> data = e;
    p -> next = nullptr;
    one -> rear -> next = p;
    one -> rear = p;
}

//删除任意部分元素
void Pop(LinkQueue* &one, int num)
{
    QueueNode* p1 = one -> front -> next;
    QueueNode* p2 = one -> front;
    while(p1 -> data != num)
    {
        p2 = p1;
        p1 = p1 -> next;
    }
    if(p1 == one -> rear)
    {
        one -> rear = p2;
        one -> rear -> next =nullptr;
        delete p1;
        p1 = nullptr;
        return;
    }
    p2 -> next = p1 -> next;
    delete p1;
    p1 = nullptr;
}

void print(LinkQueue* one)
{
    QueueNode* p = one -> front;
    while(p -> next != nullptr)
    {
        p = p -> next;
        cout << p -> data << endl;
    }
}

int main()
{
    LinkQueue* LQ;
    InitQueue(LQ);
    Push(LQ, 1);
    Push(LQ, 2);
    Push(LQ, 3);
    Push(LQ, 4);
    Push(LQ, 5);
    Push(LQ, 6);
    print(LQ);
    cout << "--------------------------------------" << endl;
    Pop(LQ, 4);
    print(LQ);
    cout << "--------------------------------------" << endl;
    Pop(LQ, 6);
    Pop(LQ, 1);
    print(LQ);
    cout << "--------------------------------------" << endl;
    Push(LQ, 7);
    Push(LQ, 8);
    print(LQ);
    return 0;
}

