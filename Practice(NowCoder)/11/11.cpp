//KMP算法(不完善)
#include<iostream>
#include<string>
using namespace std;
const int num = 1e5 + 5;
string str, pra1, pra2;
string pattern, match;
string All[num] = {"0"};
int m, x, y, count = 0;
int MAIN, KID;
int answer;

void get_next(int* next, string pat, int size)
{
    int cpas = 0;
    int i = 1;
    while (i < size)
    {
        if(pat[cpas] == pat[i])
        {
            cpas++;
            next[i] = cpas;
            i++;
        }
        else
        {
            if(cpas == 0)
            {
                next[i] = 0;
                i++;
            }
            else
                cpas = next[cpas - 1];
        }
    }
}

void KMP_judgement(int x, int y)
{
    answer = 0;
    pattern = All[x];
    match = All[y];
    int size = pattern.size();
    int next[size] = {0};
    get_next(next, pattern, size);
    MAIN = 0, KID = 0;
    while(MAIN < match.size())
    {
        if(match[MAIN] == pattern[KID])
        {
            MAIN++;
            KID++;
        }
        else if(KID > 0)
            KID = next[KID - 1];
        else
            MAIN++;
        
        if(KID == size)
            answer++;
    }
    cout << answer << endl;
}

main()
{
    cin >> str;
    for(int i = 0; i < str.size(); i++)
    {
        pra1 = str[i];
        if(str[i] == 'B')
        {
            pra2.erase(pra2.end() - 1);
            continue;
        }
        if(str[i] == 'P')
        {
            All[count] = pra2;
            count++;
            continue;
        }
        pra2 += pra1;
    }
    cin >> m;
    while(m--)
    {
        cin >> x >> y;
        KMP_judgement(x -1, y - 1);
    }
    /*
    int i = 0;
    while(All[i] != "\0")
    {
        cout << All[i] << endl;
        i++;
    }
    */
}