/*
链接：https://ac.nowcoder.com/acm/contest/48587/B
来源：牛客网

题目描述 
给你一个1->n的排列和一个栈，入栈顺序给定
你要在不打乱入栈顺序的情况下，对数组进行从大到小排序
当无法完全排序时，请输出字典序最大的出栈序列
输入描述:
第一行一个数n
第二行n个数，表示入栈的顺序，用空格隔开，结尾无空格
输出描述:
输出一行n个数表示答案，用空格隔开，结尾无空格
示例1
输入
复制
5
2 1 5 3 4
输出
复制
5 4 3 1 2
说明
2入栈；1入栈；5入栈；5出栈；3入栈；4入栈；4出栈；3出栈；1出栈；2出栈
*/
#include<iostream>
#include<stdlib.h>
using namespace std;
int n,k;
typedef struct Init
{
    int data;
    Init *next;
}I;
void InitL(I* &add)
{
    add=(struct Init*)malloc(sizeof(struct Init));
    add->data=114514;
    add->next=nullptr;
    I* P;
    while(n--)
    {
        P=(struct Init*)malloc(sizeof(struct Init));
        I* wait=P;
        cin>>P->data;
        if(P->data==k)
        {
            cout<<P->data<<" ";
            delete wait;
            k--;
            continue;
        }
        P->next=add;
        add=P;
    }
}
int main()
{
    cin>>n;
    k=n;
    I* Test;
    InitL(Test);
    while(Test->next!=nullptr)
    {
        cout<<Test->data<<" ";
        Test=Test->next;
    }
    return 0;
}