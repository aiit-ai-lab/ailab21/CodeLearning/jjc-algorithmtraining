/*
链接：https://ac.nowcoder.com/acm/contest/44065/E
来源：牛客网

题目描述 
给出一个n\times nn×n的国际象棋棋盘，你需要在棋盘中摆放nn个皇后，使得任意两个皇后之间不能互相攻击。具体来说，不能存在两个皇后位于同一行、同一列，或者同一对角线。请问共有多少种摆放方式满足条件。
输入描述:
一行，一个整数n(1\le n \le 12)n(1≤n≤12)，表示棋盘的大小。
输出描述:
输出一行一个整数，表示总共有多少种摆放皇后的方案，使得它们两两不能互相攻击。
示例1
输入
4
输出
2
*/
#include<iostream>
#include<cmath>
using namespace std;
int answer=0;
int queens[12]={0};
void calculate(int k,int n)
{
    if(k==n)
    {
        answer++;
        return;
    }
    for(int i=0;i<n;i++)
    {
        queens[k]=i;
        int j;
        for(j=0;j<k;j++)
        {
            if(queens[j]==queens[k]||abs(queens[k]-queens[j])==(k-j))
            break;
        }
        if(j==k)
        calculate(k+1,n);
    }
}
int main()
{
    int n;
    cin>>n;
    calculate(0,n);
    cout<<answer;
    return 0;   
}