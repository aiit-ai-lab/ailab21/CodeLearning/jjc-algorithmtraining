/*
链接：https://ac.nowcoder.com/acm/contest/45864/B
来源：牛客网

题目描述 

若一个数（首位不为零）从左向右读与从右向左读都一样，我们就将其称之为回文数。

例如：给定一个10进制数56，将56加65（即把56从右向左读），得到121是一个回文数。
又如：对于10进制数87：
STEP1：87+78  = 165                  STEP2：165+561 = 726

STEP3：726+627 = 1353                STEP4：1353+3531 = 4884

在这里的一步是指进行了一次N进制的加法，上例最少用了4步得到回文数4884。

写一个程序，给定一个N（2<=N<=10或N=16）进制数M（100位之内），求最少经过几步可以得到回文数。如果在30步以内（包含30步）不可能得到回文数，则输出“Impossible!”
进制N>10时，使用大写'A'字母表示10，'B'表示11,...,'E'表示16

输入描述:
两行，分别为N,M
输出描述:
STEP=ans
示例1
输入
9
87
输出
STEP=6
*/
#include<iostream>
#include<algorithm>
#include<string>
using namespace std;

bool judge(string s) {
    for(int i = 0; i < s.size() / 2; i++) {
        if(s[i] != s[s.size() - 1 - i])
            return false;
    }
    return true;
}

string add(string s, int n) {
    string t = s;
    reverse(t.begin(), t.end());
    string ans;
    for(int i = 0; i < s.size(); i++) {
        ans.push_back(s[i] + t[i] - '0');
    }
    ans.push_back('0');
    for(int i = 0; i < ans.size(); i++) {
        if(ans[i] >= (n + '0')) {
            ans[i] -= n; 
            ans[i + 1]++;
        }
    }
    while(ans.back() == '0') ans.pop_back();
    return ans;
}

int main() {
    int n;
    string m;
    cin >> n >> m;
    for(char &c : m) {
        switch(c) {
            case 'A': c = 10 + '0';
                break;
            case 'B': c = 11 + '0';
                break;
            case 'C': c = 12 + '0';
                break;
            case 'D': c = 13 + '0';
                break;
            case 'E': c = 14 + '0';
                break;
            case 'F': c = 15 + '0';
                break;
        }
    }
    
    int cnt = 0;
    while(!judge(m)) {
        if(cnt >= 30) {
            cout << "Impossible!" << endl;
            return 0;
        }
        m = add(m, n);
        cnt++;
        
    }
    cout << "STEP=" << cnt << endl;
    return 0;
}